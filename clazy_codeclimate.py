#/usr/bin/env python3

import subprocess
import json
import sys
import hashlib
import re

warnings = []

rex = "(\\/.+\\.[ch]{1,2}[px]{0,2}):(\\d+):(\\d+): (.+?): (.+) (\\[.+\\])"

# Read the stderr output from clang and convert warnings to code climate issues
# It assumes that a Makefile is present

proc = subprocess.Popen(['make'],stderr=subprocess.PIPE, stdout=subprocess.DEVNULL)
for lineb in proc.stderr:

    line = lineb.strip().decode("utf-8")

    match = re.search(rex, line)

    if not match:
        continue

    fp = hashlib.md5()
    fp.update(line.encode('utf-8'))

    warning = {
        "type": "issue",
        "description": match.group(5),
        "categories": ["Bug Risk"], # TODO
        "location": {
            "path": match.group(1),
            "lines": {
                "begin": match.group(2)
            }
        },
        "fingerprint": fp.hexdigest()
    }

    warnings.append(warning)

f = open("report.json", "w")
f.write(json.dumps(warnings))
f.close()
